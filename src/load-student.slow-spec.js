const { assertThat, equalTo } = require('hamjest');
const { loadStudent } = require('./load-student');

describe('Load a student #slowtest', () => {
  it('is all wired up and works (against the real backend)', () => {
    const student = loadStudent({ studentId: 42 });
    assertThat(student, equalTo('from the backend'));
  });
});
