const { assertThat, equalTo } = require('hamjest');
const { loadStudent } = require('./load-student');

describe('Load a student', () => {
  it('from the cache', () => {
    let dependencies = { cache: { 42: 'student 42' } };
    const student = loadStudent({ studentId: 42 }, dependencies);
    assertThat(student, equalTo('student 42'));
  });
  it('fetch the student if not in the cache', () => {
    const emptyCache = {};
    const dependencies = { cache: emptyCache, fetch: () => 'student 23' };
    const student = loadStudent({ studentId: 23 }, dependencies);
    assertThat(student, equalTo('student 23'));
  });
});
