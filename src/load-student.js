const { fetch } = require('./backend');

const cache = {};
const defaultDependencies = () => {
  return { fetch, cache };
};

const loadStudent = ({ studentId }, dependencies = defaultDependencies()) => {
  const cache = dependencies.cache;
  const fetch = dependencies.fetch;
  if (!cache[studentId]) {
    cache[studentId] = fetch({ studentId });
  }
  return cache[studentId];
};

module.exports = { loadStudent };
